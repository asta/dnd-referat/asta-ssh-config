#!/bin/sh

##################### FUNCTIONS #####################

read_custom_env() {
	if [ -f "${ASTASSHCUSTOMENVFILE}" ]; then
		. "${ASTASSHCUSTOMENVFILE}"
	fi
}

update_known_hosts() {
	echo "@cert-authority * $(curl "${ASTASSHHOSTCAPUBKEYURL}")" > "${ASTASSHKNWONHOSTSFILE}"
}

update_file_permissions() {
	if [ -f "${ASTASSHCUSTOMENVFILE}" ]; then
		chmod 600 "${ASTASSHCUSTOMENVFILE}"
	fi
	chmod 700 "${ASTASSHCONFIGDIR}/scripts/auth.sh"
	chmod 600 "${ASTASSHCONFIGFILE}"
	chmod 600 "${ASTASSHKNWONHOSTSFILE}"
	chmod 600 "${ASTASSHGENERATEDCONFIGFILE}"
}

generate_ssh_config() {
	# Delete old
	echo "# This file is manged by update.sh and generated from ${ASTASSHSERVERSCSVURL}"
	echo "# DO NOT EDIT BY HAND!"
	echo ""
	serverscsv="$(curl "${ASTASSHSERVERSCSVURL}" | tr -d ' ' | grep '^t')"
	echo "$serverscsv" | while read serverline ; do
		#echo "$serverline"
		SERVERNAME="$(echo "${serverline}" | cut -d '|' -f 2)"
		SERVERIP="$(echo "${serverline}" | cut -d '|' -f 3)"
		SERVERDOMAINS="$(echo "${serverline}" | cut -d '|' -f 4)"
		SERVERDOMAIN="$(echo "${SERVERDOMAINS}" | cut -d ',' -f 1)"
		#SERVERPRINCIPALS="admin\n${SERVERNAME}\n$(echo "${serverline}" | cut -d '|' -f 5 | tr ',' '\n')"
	
		echo "Host ${SERVERNAME}"
		echo "	HostName ${SERVERDOMAIN}"
		echo "	User cloud"
		echo "	UserKnownHostsFile \"${ASTASSHKNWONHOSTSFILE}\""
		echo "	CertificateFile \"${ASTAPUBKEYCERTFILE}\""
		echo "	IdentityAgent \"${SSH_AUTH_SOCK}\""
		echo ""
	done
}

update_git_repos() {
	cd "${ASTASSHCONFIGDIR}"
	git pull
	cd - > /dev/null
}

##################### MAIN #####################

# Exit on error
set -e

SSHCONFIGDIR="${SSHCONFIGDIR:=${HOME}/.ssh}"
SSHCONFIGFILE="${SSHCONFIGFILE:=${SSHCONFIGDIR}/config}"
ASTASSHCONFIGDIR="${ASTASSHCONFIGDIR:=${SSHCONFIGDIR}/asta-ssh-config}"
ASTASSHCONFIGFILE="${ASTASSHCONFIGFILE:=${ASTASSHCONFIGDIR}/config}"
ASTASSHCUSTOMENVFILE="${SSHCUSTOMENVFILE:=${ASTASSHCONFIGDIR}/.env}"

ASTASSHKNWONHOSTSFILE="${ASTASSHCONFIGDIR}/known_hosts"
ASTASSHHOSTCAPUBKEYURL="${ASTASSHHOSTCAPUBKEYURL:=https://sshca.asta.uni-goettingen.de/host-ca.pub}"

KEYNAME="id_astassh"
ASTAPUBKEYCERTFILE="${ASTASSHCONFIGDIR}/${KEYNAME}-cert.pub"

ASTASSHSERVERSCSVURL="${ASTASSHSERVERSCSVURL:=https://asta.pages.gwdg.de/dnd-referat/asta-ansible/ssh_ca_servers.csv}"

ASTASSHGENERATEDCONFIGFILE="${ASTASSHCONFIGFILE}.generated"

SSH_AUTH_SOCK="${ASTASSHCONFIGDIR}/session-sock"

ASTASSHGITURL="${ASTASSHGITURL:=https://gitlab.gwdg.de/asta/dnd-referat/asta-ssh-config.git}"

# Read user specified custom settings
read_custom_env

echo "=> Updating AStA SSH Config"

echo "=> Updating git repos"
update_git_repos

echo "=> Updating known_hosts"
update_known_hosts

echo "=> Generating SSH config"
generate_ssh_config > "${ASTASSHGENERATEDCONFIGFILE}"

echo "=> Updating permissions"
update_file_permissions

echo "=> Finished updating AStA SSH Config"
# Disable exit on error
set +e
