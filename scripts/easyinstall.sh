#!/bin/sh

##################### FUNCTIONS #####################

check_dependency() {
	if ! command -v "${1}" > /dev/null ; then
		echo "Error: Command \"${1}\" not found. Please install ${2}."
		exit 1
	fi
}

check_dependencies() {
	check_dependency test "the GNU Coreutitls"
	check_dependency cat "the GNU Coreutitls"
	check_dependency tr "the GNU Coreutitls"
	check_dependency id "the GNU Coreutitls"
	check_dependency stat "the GNU Coreutitls"
	check_dependency curl "Curl"
	check_dependency git "Git"
	check_dependency ssh-keygen "the OpenSSH Client"
	check_dependency ssh-add "the OpenSSH Client"
	check_dependency ssh-agent "the OpenSSH Client"
}

# easyinstall.sh must not read from stdin! 
#ask_yon() {
#	while true; do
#		printf "(Y/N): "
#		read -r YON
#		YON="$(echo "${YON}" | tr '[:upper:]' '[:lower:]')"
#		if [ "x${YON}" = "xy" ] || [ "x${YON}" = "xyes" ]; then
#			true; return
#		elif [ "x${YON}" = "xn" ] || [ "x${YON}" = "xno" ]; then
#			false; return
#		fi
#	done
#}

check_dirs_and_files() {
	if [ ! -d "${SSHCONFIGDIR}" ]; then
		echo "SSH config directory not found. Creating \"${SSHCONFIGDIR}\"."
		mkdir -p "${SSHCONFIGDIR}"
	fi
	if [ ! -f "${SSHCONFIGFILE}" ]; then
		echo "SSH config file not found. Creating \"${SSHCONFIGFILE}\"."
		touch "${SSHCONFIGFILE}"
	fi
	if [ "x$(stat -c '%u' "${SSHCONFIGFILE}")" != "x$(id -u)" ]; then
		echo "Warn: You are not the owner of your SSH config file!"
		echo "If you are on the CIP Pool computers of the Institute of Computer Science see https://gitlab.gwdg.de/cip/pool-bugs/-/issues/97 ."
		echo "You might wish to contact Jake for further help."
		echo ""
	fi
	if [ -d "${ASTASSHCONFIGDIR}" ]; then
		echo "Error: \"${ASTASSHCONFIGDIR}\" already exists. Refusing to overwrite!"
		echo ""
		echo "=> Running update.sh instead"
		sh -c "${ASTASSHUPDATEFILE}"

		exit 1
	fi
}


##################### MAIN #####################

main() {

	# Exit on error
	set -e
	
	
	SSHCONFIGDIR="${SSHCONFIGDIR:=${HOME}/.ssh}"
	SSHCONFIGFILE="${SSHCONFIGFILE:=${SSHCONFIGDIR}/config}"
	ASTASSHCONFIGDIR="${ASTASSHCONFIGDIR:=${SSHCONFIGDIR}/asta-ssh-config}"
	ASTASSHCONFIGFILE="${ASTASSHCONFIGFILE:=${ASTASSHCONFIGDIR}/config}"
	ASTASSHUPDATEFILE="${ASTASSHCONFIGDIR}/scripts/update.sh"
	ASTASSHENVFILE="${ASTASSHCONFIGDIR}/.env"
	ASTASSHENVEXAMPLEFILE="${ASTASSHCONFIGDIR}/.env.example"
	ASTASSHGITURL="${ASTASSHGITURL:=https://gitlab.gwdg.de/asta/dnd-referat/asta-ssh-config.git}"
	
	echo "=> Installing AStA SSH Config"
	echo "Target directory: ${ASTASSHCONFIGDIR}"

	echo "=> Checking dependencies"
	check_dependencies
	
	echo "=> Checking files and directories"
	check_dirs_and_files
	
	echo "=> Cloning git repo"
	git clone "${ASTASSHGITURL}" "${ASTASSHCONFIGDIR}"

	echo "=> Updating SSH config"
	if [ "x$(grep -F "asta-ssh-config" "$SSHCONFIGFILE")" = "x" ]; then
		echo "Match originalhost \"asta-*\"" >> "${SSHCONFIGFILE}"
		echo "	Include ${ASTASSHCONFIGFILE}" >> "${SSHCONFIGFILE}"
	fi

	if [ ! -f "${ASTASSHENVFILE}" ]; then
		echo "=> Creating .env file"
		cp "${ASTASSHENVEXAMPLEFILE}" "${ASTASSHENVFILE}"
	fi

	echo "=> Running update.sh"
	sh -c "${ASTASSHUPDATEFILE}"

	echo ""
	echo "=> Installation of AStA SSH Config was successful!"
	echo ""
	echo "You can find all related files in ${ASTASSHCONFIGDIR}"
	echo "Please don't forget to run update.sh regularly using:"
	echo "sh ${ASTASSHUPDATEFILE}"
	echo ""
	echo "You may now type 'ssh asta-' and hit <TAB> twice to see all servers."

	# Disable exit on error
	set +e
	
}

main

