#!/bin/sh

##################### FUNCTIONS #####################

read_custom_env() {
	if [ -f "${ASTASSHCUSTOMENVFILE}" ]; then
		. "${ASTASSHCUSTOMENVFILE}"
	fi
}

make_secure_temp_dir() {
	TMPDIR="/dev/shm"
	if [ ! -e "${TMPDIR}" ]; then
		echo "Warn: TMPDIR: ${TMPDIR} not found! Using /tmp instead."
		TMPDIR="/tmp"
	fi
	SECURETEMPDIR="$(mktemp -d -p "${TMPDIR}" sshsectmp.XXXXXXXXXX)"
	#echo "TEMPDIR: "${SECURETEMPDIR}""
}

remove_secure_temp_dir() {
	if [ "x" != "x${SECURETEMPDIR}" ]; then
		rm -rf "${SECURETEMPDIR}"
	fi
}

started_ssh_agent() {



	if [ "x" != "x${ASTASSH_AGENT_PID}" ]; then
		#echo "[DEBUG] ASTASSH_AGENT_PID \"${ASTASSH_AGENT_PID}\" is already defined -> write it to ASTASSH_AGENT_PID_FILE"
		printf "%s" "${ASTASSH_AGENT_PID}" > "${ASTASSH_AGENT_PID_FILE}"
	fi

	if is_ssh_agent_running ; then
		#echo "[DEBUG] agent already running, don't need to start a new one"
		return
	fi
	
	#echo "[DEBUG] ASTASSH_AUTH_SOCK: \"${ASTASSH_AUTH_SOCK}\""
	if [ -e "${ASTASSH_AUTH_SOCK}" ]; then
		echo "Warn: ssh-agent socket file exists, but no agent running."
		echo "Perhaps the old agent died? Removing old socket file."
		rm -f "${ASTASSH_AUTH_SOCK}"
	fi

	export SSH_AGENT_PID="${ASTASSH_AGENT_PID}"
	export SSH_AUTH_SOCK="${ASTASSH_AUTH_SOCK}"
	# Start new SSH agent
	AGENTOUTPUT="$(ssh-agent -s -a "${ASTASSH_AUTH_SOCK}")"
	#echo "[DEBUG] ASTASSH_AUTH_SOCK: \"${ASTASSH_AUTH_SOCK}\""
	# run ssh-agent first and then eval the output so that errors can be detected correctly
	eval "${AGENTOUTPUT}"
	export ASTASSH_AGENT_PID="${SSH_AGENT_PID}"
	export ASTASSH_AUTH_SOCK="${SSH_AUTH_SOCK}"
	# write PID to file
	printf "%s" "${ASTASSH_AGENT_PID}" > "${ASTASSH_AGENT_PID_FILE}"
	# make sure you can read the pid back
	read_ssh_agent_pid_from_file
}

read_ssh_agent_pid_from_file() {
	export ASTASSH_AGENT_PID="$(cat "${ASTASSH_AGENT_PID_FILE}")"
}

is_ssh_agent_running() {
	if [ ! -e "${ASTASSH_AUTH_SOCK}" ]; then
		#echo "[DEBUG] ASTASSH_AUTH_SOCK doesn't exist"
		false; return
	elif [ ! -f "${ASTASSH_AGENT_PID_FILE}" ]; then
		#echo "[DEBUG] ASTASSH_AGENT_PID_FILE doesn't exist"
		false; return
	fi

	read_ssh_agent_pid_from_file
	if [ ! -e "/proc/${ASTASSH_AGENT_PID}" ]; then
		#echo "[DEBUG] no session running at PID \"${ASTASSH_AGENT_PID}\" (dir: \"/proc/${ASTASSH_AGENT_PID}\")"
		false; return
	fi

	#echo "[DEBUG] Session is running" 1
	true; return
}

open_browser() {
	if command -v xdg-open > /dev/null ; then
		xdg-open "${1}" &
	elif command -v gnome-open > /dev/null ; then
		gnome-open "${1}" &
	elif command -v open > /dev/null ; then
		# MacOS
		open "${1}" &
	else
		false; return
	fi

	return
}

auth_and_get_jwt() {

	# Open login URL
	if [ "x${ASTASSHOPENBROWSERMANUALLY}" = "xtrue" ]; then
		echo ""
		echo "Please open the following URL in your web browser:"
		echo ""
		echo "${ASTASSHLOGINURL}"
		echo ""
	else
		echo ""
		echo "Your default web browser has been opened to visit:"
		echo ""
		echo "${ASTASSHLOGINURL}"
		echo ""

		# Open Browser with $ASTASSHLOGINURL
		if ! open_browser "${ASTASSHLOGINURL}" ; then
			echo "Warn: Failed opening Browser. Please open the URL manually."
			echo ""
		fi
	fi


	echo "Please login and paste the resulting Token here."
	echo "(Note: Input is not echoed back on the command line.)"

	# Disable exit on error as the user might press Ctrl+c
	set +e

	# Read JWT
	## see https://unix.stackexchange.com/questions/222974/ask-for-a-password-in-posix-compliant-shell
	JWT="$(
		# always read from the tty even when redirected:
		exec < /dev/tty || exit # || exit only needed for bash
		# save current tty settings:
		tty_settings=$(stty -g) || exit
		# schedule restore of the settings on exit of that subshell
		# or on receiving SIGINT or SIGTERM:
		trap 'stty "$tty_settings"' EXIT INT TERM
		# disable terminal local echo
		stty -echo || exit
		# prompt on tty
		printf "Token: " > /dev/tty
		# read password as one line, record exit status
		IFS= read -r password; ret=$?
		# display a newline to visually acknowledge the entered password
		echo > /dev/tty
		# return the password for $REPLY
		printf '%s\n' "$password"
		exit "$ret"
	)"

	# Reenable exit on error
	set -e

	echo ""
	echo ""

	##echo "[DEBUG] JWT: \"${JWT}\""

}


##################### MAIN #####################

main() {

	# Exit on error
	set -e
	
	#echo "[DEBUG] ENV 1:"
	#set +e
	#env | grep SSH
	#set -e
	
	SSHCONFIGDIR="${SSHCONFIGDIR:=${HOME}/.ssh}"
	ASTASSHCONFIGDIR="${ASTASSHCONFIGDIR:=${SSHCONFIGDIR}/asta-ssh-config}"
	ASTASSHCUSTOMENVFILE="${SSHCUSTOMENVFILE:=${ASTASSHCONFIGDIR}/.env}"
	
	ASTASSHOPENBROWSERMANUALLY="${ASTASSHOPENBROWSERMANUALLY:=false}"
	
	ASTASSHLOGINURL="https://sshca.asta.uni-goettingen.de/login"
	ASTASSHCERTIFYURL="https://sshca.asta.uni-goettingen.de/cgi/certifykey"
	
	ASTASSHCERTTTL="${ASTASSHCERTTTL:=12h}"
	#ASTASSHCERTTTL="${ASTASSHCERTTTL:=1m}"
	
	# Read user specified custom settings
	read_custom_env
	
	ASTASSH_AGENT_PID_FILE="${ASTASSHCONFIGDIR}/session-pid"
	#export ASTASSH_AUTH_SOCK="${ASTASSH_AUTH_SOCK:=${ASTASSHCONFIGDIR}/session-sock}"
	export ASTASSH_AUTH_SOCK="${ASTASSHCONFIGDIR}/session-sock"
	
	#echo "[DEBUG] ENV 2:"
	#set +e
	#env | grep SSH
	#set -e

	started_ssh_agent

	#echo "[DEBUG] ENV 3:"
	#set +e
	#env | grep SSH
	#set -e
	
	
	KEYCOMMENT="asta-ssh-key"
	KEYNAME="id_astassh"
	
	export SSH_AGENT_PID="${ASTASSH_AGENT_PID}"
	export SSH_AUTH_SOCK="${ASTASSH_AUTH_SOCK}"
	# Test if a key already exists
	#echo "[DEBUG] Test if a key already exists"
	#echo "[DEBUG] SSH_AGENT_PID: \"${SSH_AGENT_PID}\""
	#echo "[DEBUG] SSH_AUTH_SOCK: \"${SSH_AUTH_SOCK}\""
	if ! ssh-add -l | grep " ${KEYCOMMENT} " > /dev/null ; then
		# Key not found. -> Requesting a new one
		#echo "[DEBUG] No Key found. Requesting a new one..."
	
		make_secure_temp_dir
		
		JWTAUTHHEADERFILE="${SECURETEMPDIR}/jwtauthheader"
		PRIVKEYFILE="${SECURETEMPDIR}/${KEYNAME}"
		PUBKEYFILE="${PRIVKEYFILE}.pub"
		RESPUBKEYFILE="${PRIVKEYFILE}-cert.pub"
		FINPUBKEYCERTFILE="${ASTASSHCONFIGDIR}/${KEYNAME}-cert.pub"
		
		auth_and_get_jwt
		
		# Store JWT as file so that curl can reference it and it doesn't have to show up in ps
		# (printf is a shell builtin and thus not listed in ps)
		printf "Authorization: Bearer %s" "${JWT}" > "${JWTAUTHHEADERFILE}"
		
		ssh-keygen -t ed25519 -N '' -C "${KEYCOMMENT}" -f "${PRIVKEYFILE}"
		
		echo ""
		
		# Certify Key
		RESPCODE="$(curl -o "${RESPUBKEYFILE}" -s -w "%{http_code}\n" -X POST -H "Content-Type: text/plain" -H "@${JWTAUTHHEADERFILE}" -d "@${PUBKEYFILE}" "${ASTASSHCERTIFYURL}")"
		if [ "x${RESPCODE}" != "x200" ]; then
			echo "Error: Failed cerifying key:"
			# RESPUBKEYFILE contains the error
			cat "${RESPUBKEYFILE}"
			exit 1
		fi
		
		# JWT is no longer needed
		JWT=""

		export SSH_AGENT_PID="${ASTASSH_AGENT_PID}"
		export SSH_AUTH_SOCK="${ASTASSH_AUTH_SOCK}"
		ssh-add -t "${ASTASSHCERTTTL}" "${PRIVKEYFILE}"
	
		cp -f "${RESPUBKEYFILE}" "${FINPUBKEYCERTFILE}"
		chmod 600 "${FINPUBKEYCERTFILE}"
		
		remove_secure_temp_dir

		echo ""
		echo "==>> Authentication successful."
		echo ""
	#	else
		#echo "[DEBUG] Valid Key found."
	fi
	
	export SSH_AGENT_PID="${ASTASSH_AGENT_PID}"
	export SSH_AUTH_SOCK="${ASTASSH_AUTH_SOCK}"
	#echo "[DEBUG] Reached end."
	
	# Disable exit on error
	set +e
	
}

# This program is mainly run by the ssh command through the config. Thus all output must be redirected to STDERR.
main 1>&2

