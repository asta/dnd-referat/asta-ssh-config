# AStA SSH Config
SSH Config+Skripte um die Server des AStAs zu administrieren.

[[_TOC_]]

## FAQ

### Welche Software muss auf meinem System vorinstalliert sein? Welche sonstigen Voraussetzungen sollte mein System erfüllen?

Die folgende Software muss auf deinem System installiert sein:

- [GNU Coreutils](https://www.gnu.org/software/coreutils/)
	- `test`
	- `cat`
	- `tr`
	- `id`
	- `stat`
- [Curl](https://curl.se/)
	- `curl`
- [Git](https://git-scm.com/)
	- `git`
- [OpenSSH Client](https://www.openssh.com/)
	- `ssh-keygen`
	- `ssh-add`
	- `ssh-agent`

Zusätzlich musst du auf einem [weitgehend POSIX-kompatiblem Betriebssystem](https://de.wikipedia.org/wiki/Portable_Operating_System_Interface#POSIX-kompatible_Betriebssysteme) sein.
(Unter Windows kannst du einfach das [WSL](https://de.wikipedia.org/wiki/Windows-Subsystem_f%C3%BCr_Linux) verwenden.)

Des Weiteren ist es notwendig, dass `/bin/sh` auf eine POSIX-kompatible Shell zeigt!

Es ist zusätzlich stark empfohlen einen [SSH Key mit dem eigenen GitLab Account](https://docs.gitlab.com/ee/ssh/) verknüpft zu haben.

### Wie installiere ich diese Config bei mir?

Führe einfach das easyinstall-Skript mit dem folgenden Befehl aus:
```
curl https://asta.pages.gwdg.de/dnd-referat/asta-ssh-config/easyinstall.sh | sh
```

Das easyinstall-Skript führt die folgenden Schritte durch:
1. Abhängigkeiten überprüfen
1. `~/.ssh` Ordner überprüfen
1. Repo klonen & aufbereiten
1. SSH Config aktualisieren
1. `.env` Datei erstellen
1. `update.sh` ausführen

Das easyinstall-Skript installiert dieses Projekt nach `~/.shh/asta-ssh-config`.
Außerdem fügt das easyinstall-Skript die Zeile `Include ~/.ssh/asta-ssh-config/config` in `~/.ssh/config` hinzu.


(Falls du diese Config auf einem CIP Pool Rechner des Instituts der Informatik einrichten willst, kann es zu Permissionproblemen kommen. Siehe cip/pool-bugs#97 )

#### Und wie sollte die Ausgabe von dem easyinstall-Skript aussehen?
In etwa so:
```
$ curl https://asta.pages.gwdg.de/dnd-referat/asta-ssh-config/easyinstall.sh | sh
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  3521  100  3521    0     0  83833      0 --:--:-- --:--:-- --:--:-- 90282
=> Installing AStA SSH Config
Target directory: /home/user/.ssh/asta-ssh-config
=> Checking dependencies
=> Checking files and directories
SSH config file not found. Creating "/home/user/.ssh/config".
=> Cloning git repo
Cloning into '/home/user/.ssh/asta-ssh-config'...
remote: Enumerating objects: 97, done.
remote: Counting objects: 100% (97/97), done.
remote: Compressing objects: 100% (60/60), done.
Receiving objects: 100% (112/112), 21.59 KiB | 4.32 MiB/s, done.
remote: Total 112 (delta 51), reused 71 (delta 34), pack-reused 15
Resolving deltas: 100% (53/53), done.
Submodule 'ssh-ca' (git@gitlab.gwdg.de:asta/dnd-referat/ssh-ca.git) registered for path 'ssh-ca'
Cloning into '/home/user/.ssh/asta-ssh-config/ssh-ca'...
Submodule path 'ssh-ca': checked out '841e18670297f2be14a91059f13b64516b330848'
=> Updating SSH config
=> Creating .env file
=> Running update.sh
=> Updating AStA SSH Config
=> Updating git repos
Already up to date.
=> Updating known_hosts
=> Generating SSH config
=> Updating permissions
=> Finished updating AStA SSH Config

=> Installation of AStA SSH Config was successful!

You can find all related files in /home/user/.ssh/asta-ssh-config
Please don't forget to run update.sh regularly using:
sh /home/user/.ssh/asta-ssh-config/scripts/update.sh

You may now type 'ssh asta-' and hit <TAB> twice to see all servers.

```


### Wie logge ich mich auf einem Server ein?

Nutze einfach `ssh asta-<Host>`, wobei `<Host>` der Title des Servers, so wie er in der [IT-Dokumentation](https://gitlab.gwdg.de/asta/dnd-referat/it-dokumentation/-/wikis/servers) steht, ist.

Falls du dich noch nicht authentifiziert hast, wirst du nach einem Token gefragt und dein Browser wird geöffnet, damit du dich anmelden kannst.

Beispiel:
```
$ ssh asta-test01
Agent pid 82622

Your default web browser has been opened to visit:

https://sshca.asta.uni-goettingen.de/login

Please login and paste the resulting Token here.
(Note: Input is not echoed back on the command line.)
Token:


Generating public/private ed25519 key pair.
Your identification has been saved in /dev/shm/sshsectmp.bROA5eqrS2/id_astassh
Your public key has been saved in /dev/shm/sshsectmp.bROA5eqrS2/id_astassh.pub
The key fingerprint is:
SHA256:kcTXBC4OvXLDGxqsD7shhhFNj8+s4ibWNcA4B8O8gV0 asta-ssh-key
The key's randomart image is:
+--[ED25519 256]--+
|+...E  .. .+.    |
|.O.o   o.o. .    |
|. X . . =..      |
| = B . + +       |
|. o = + S        |
| o . + = +       |
|o = = o .        |
|o= o =           |
|+.  o..          |
+----[SHA256]-----+

Identity added: /dev/shm/sshsectmp.bROA5eqrS2/id_astassh (asta-ssh-key)
Lifetime set to 43200 seconds
Certificate added: /dev/shm/sshsectmp.bROA5eqrS2/id_astassh-cert.pub (111d3612-38fa-4c7b-9218-27f0f751f704)
Lifetime set to 43200 seconds

==>> Authentication successful.

Welcome to Ubuntu 20.04.2 LTS (GNU/Linux 5.4.0-66-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

2 updates can be applied immediately.
To see these additional updates run: apt list --upgradable

*** System restart required ***
Last login: Mon Jul 12 04:48:40 2021 from 134.76.81.36
cloud@test-01:~$
```

`ssh` unterstützt in den meisten Shells auch Autovervollständigung.
In Bash z.B. kann man einfach nur `ssh asta-` schreiben und dann zweimal die `TAB`-Taste drücken um alle Server aufgelistet zu bekommen.


### Wie aktualisiere ich die Config?
Wenn ein neuer Server hinzugefügt wurde, muss man die SSH Config aktualisieren.

Dafür kann man einfach den folgenden Befehl ausführen:
```
~/.ssh/asta-ssh-config/scripts/update.sh
```

### Woher weiß die Config welche Server existieren?

`update.sh` generiert die Config basierend auf den Daten von [https://asta.pages.gwdg.de/dnd-referat/asta-ansible/ssh_ca_servers.csv](https://asta.pages.gwdg.de/dnd-referat/asta-ansible/ssh_ca_servers.csv) .

### Wie fügt man einen neuen Server hinzu?

Siehe [hier](https://gitlab.gwdg.de/asta/dnd-referat/it-dokumentation/-/wikis/Neuen%20Server%20aufsetzen).

### Muss ich mich bei jedem Login neu authentifizieren? Bzw. woher weiß SSH ob ich schon authentifiziert bin?
Sobald du dich authentifiziert hast, wird ein SSH Key generiert und zertifiziert.
Dieser SSH Key+Zertifikat werden in einer ssh-agent Session gespeichert.

Solange ein SSH Key+Zertifikat im ssh-agent gespeichert ist, solange kann man sich auch, ohne sich neu zu authentifizieren, auf Servern einloggen.

Standardmäßig beduetet das, dass man sich nur alle 12 Stunden neu authentifizieren muss.

### Wird meine eigene ssh-agent Session bei AStA Servern beachtet?
Nein.

Die AStA SSH Config wird immer eine eigene ssh-agent Session verwenden.

Dies lässt sich aus technischen Gründen auch nicht ändern.


### Wie funktioniert das Ganze? Welche Technologien werden verwendet?
TODO
kurz: SSH Zertifikate

### Muss ich immer die `asta-` Schreibweise verwenden oder kann ich auch die Domain von dem Server direkt verwenden?

Nein. Die `asta-` Schreibweise muss verwendet werden, da sonst nicht die korrekte ssh-agent Session verwendet wird.

### Wie kann ich verhindern, dass beim Authentifiziern mein Browser geöffnet wird?

Dieses Verhalten kannst du in der `~/.ssh/asta-ssh-config/.env`-Datei anpassen.

Siehe [hier](.env.example) für weitere Details.

### Wie kann ich mich "ausloggen"?

Einfach den ssh-agent killen: `killall ssh-agent`

